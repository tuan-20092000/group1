﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.Common.StatusTasks
{
    public enum StatusTask
    {
        ToDo,
        Doing,
        Done
    }
}
