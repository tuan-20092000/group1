﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectScheduleManagement.APIs.Migrations
{
    public partial class TaskDetailUserNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskDetails_AspNetUsers_UserId",
                table: "TaskDetails");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TaskDetails",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 6, 16, 22, 52, 22, 838, DateTimeKind.Local).AddTicks(8383),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 6, 16, 22, 10, 37, 654, DateTimeKind.Local).AddTicks(2562));

            migrationBuilder.AddForeignKey(
                name: "FK_TaskDetails_AspNetUsers_UserId",
                table: "TaskDetails",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskDetails_AspNetUsers_UserId",
                table: "TaskDetails");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TaskDetails",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 6, 16, 22, 10, 37, 654, DateTimeKind.Local).AddTicks(2562),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 6, 16, 22, 52, 22, 838, DateTimeKind.Local).AddTicks(8383));

            migrationBuilder.AddForeignKey(
                name: "FK_TaskDetails_AspNetUsers_UserId",
                table: "TaskDetails",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
