﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectScheduleManagement.APIs.Migrations
{
    public partial class AddImageAndExtendAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 6, 15, 10, 8, 45, 144, DateTimeKind.Local).AddTicks(7705),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 6, 14, 21, 20, 46, 330, DateTimeKind.Local).AddTicks(389));

            migrationBuilder.AddColumn<string>(
                name: "Extend",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "AspNetUsers",
                type: "varbinary(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Extend",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 6, 14, 21, 20, 46, 330, DateTimeKind.Local).AddTicks(389),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 6, 15, 10, 8, 45, 144, DateTimeKind.Local).AddTicks(7705));
        }
    }
}
