﻿using AutoMapper;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Projects;
using ProjectScheduleManagement.DTO.TaskDetails;
using ProjectScheduleManagement.DTO.Tasks;

namespace ProjectScheduleManagement.APIs.Mapper
{
    public class MappingProfileSpecific : Profile
    {
        public MappingProfileSpecific()
        {
            CreateMap<Project, ProjectInsertDTO>().ReverseMap();
            CreateMap<Project, ProjectUpdateDTO>().ReverseMap();
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Task, TaskDisplayDTO>().ReverseMap();
            CreateMap<Task, TaskCreateDTO>().ReverseMap();
            CreateMap<Task, TaskUpdateDTO>().ReverseMap();
            CreateMap<TaskDetail, TaskDetailDisplayDTO>().ReverseMap();
            CreateMap<TaskDetail, TaskDetailCreateDTO>().ReverseMap();
            CreateMap<TaskDetail, TaskDetailUpdateDTO>().ReverseMap();
        }
    }
}
