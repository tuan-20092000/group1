﻿using AutoMapper;

namespace ProjectScheduleManagement.APIs.Mapper
{
    public static class MappingProfileGeneral<TSource, TDestination>
	{
		private static AutoMapper.Mapper _myMapper = new AutoMapper.Mapper
			(new MapperConfiguration(config => config.CreateMap<TSource, TDestination>().ReverseMap()));
		public static TDestination Map(TSource source)
		{
			return _myMapper.Map<TDestination>(source);
		}
	}
}
