﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.APIs.Configurations
{
    public class JwtConfig
    {
        public string Secret { get; set; }
    }
}
