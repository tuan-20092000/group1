﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectScheduleManagement.APIs.Services.ProjectServices;
using ProjectScheduleManagement.DTO.Projects;
using System;

namespace ProjectScheduleManagement.APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            var response = _projectService.GetAll();
            return Ok(response.Data);
        }

        [HttpGet("{id:Guid}", Name = "GetById")]
        [AllowAnonymous]
        public IActionResult GetByID(Guid id)
        {
            var response = _projectService.GetById(id);
            return Ok(response.Data);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ProjectInsertDTO projectInsertDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _projectService.Add(projectInsertDTO);
            return Ok(response.Data);
            //return CreatedAtRoute("GetById", new { id = response.Data. }, response.Data);
        }

        [HttpPost("{key}")]
        public IActionResult Update(Guid key, [FromBody] ProjectUpdateDTO projectUpdateDTO)
        {
            if (projectUpdateDTO is null || projectUpdateDTO.Id != key)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _projectService.Update(projectUpdateDTO);
            return NoContent();
        }

        [HttpDelete("{key}")]
        public IActionResult Delete(Guid key)
        {
            var response = _projectService.Delete(key);
            if (response is null)
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }
    }
}
