﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectScheduleManagement.APIs.Services.TaskServices;
using ProjectScheduleManagement.DTO.Tasks;
using System;

namespace ProjectScheduleManagement.APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("{projectId}")]
        [AllowAnonymous]
        public IActionResult GetAll(string projectId)
        {
            var response = _taskService.GetAll(projectId);
            return Ok(response.Data);
        }

        [HttpGet("{id:Guid}", Name = "GetTaskById")]
        [AllowAnonymous]
        public IActionResult GetByID(Guid id)
        {
            var response = _taskService.GetById(id);
            return Ok(response.Data);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskCreateDTO taskCreateDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _taskService.Add(taskCreateDTO);
            return Ok(response.Data);
        }

        [HttpPut]
        public IActionResult Update(Guid id, [FromBody] TaskUpdateDTO taskUpdateDTO)
        {
            if (taskUpdateDTO is null || taskUpdateDTO.Id != id)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _taskService.Update(taskUpdateDTO);
            return CreatedAtRoute("GetTaskById", new { id = response.Data.Id }, response.Data);
        }

        [HttpDelete]
        public IActionResult Delete(Guid id)
        {
            var response = _taskService.Delete(id);
            if (response is null)
            {
                return NotFound();
            }
            if (response.StatusCode is System.Net.HttpStatusCode.NotFound)
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
