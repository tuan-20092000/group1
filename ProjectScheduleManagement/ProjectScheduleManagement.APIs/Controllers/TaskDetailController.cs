﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectScheduleManagement.APIs.Services.TaskDetailServices;
using ProjectScheduleManagement.DTO.TaskDetails;
using System;

namespace ProjectScheduleManagement.APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TaskDetailController : ControllerBase
    {
        private readonly ITaskDetailService _taskDetailService;

        public TaskDetailController(ITaskDetailService taskDetailService)
        {
            _taskDetailService = taskDetailService;
        }

        [HttpGet("{taskId}")]
        [AllowAnonymous]
        public IActionResult GetAll(string taskId)
        {
            var response = _taskDetailService.GetAll(taskId);
            return Ok(response.Data);
        }

        [HttpGet("{id:Guid}", Name = "GetTaskDetailById")]
        [AllowAnonymous]
        public IActionResult GetByID(Guid id)
        {
            var response = _taskDetailService.GetById(id);
            return Ok(response.Data);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskDetailCreateDTO taskDetailCreateDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _taskDetailService.Add(taskDetailCreateDTO);
            return Ok(response.Data);
        }

        [HttpPut]
        public IActionResult Update(Guid id, [FromBody] TaskDetailUpdateDTO taskDetailUpdateDTO)
        {
            if (taskDetailUpdateDTO is null || taskDetailUpdateDTO.Id != id)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var response = _taskDetailService.Update(taskDetailUpdateDTO);
            return CreatedAtRoute("GetTaskDetailById", new { id = response.Data.Id }, response.Data);
        }

        [HttpDelete]
        public IActionResult Delete(Guid id)
        {
            var response = _taskDetailService.Delete(id);
            if (response is null)
            {
                return NotFound();
            }
            if (response.StatusCode is System.Net.HttpStatusCode.NotFound)
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
