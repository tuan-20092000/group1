﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProjectScheduleManagement.APIs.Configurations;
using ProjectScheduleManagement.APIs.Services.AppUserServices;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.AppUsers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AppUserController : ControllerBase
    {
        private readonly IAppUserService _appUserService;
        private readonly JwtConfig _optionsMonitor;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;
        public AppUserController(IAppUserService appUserService, IOptionsMonitor<JwtConfig> optionsMonitor, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _appUserService = appUserService;
            _optionsMonitor = optionsMonitor.CurrentValue;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost]
        public IActionResult Register(AppUserRegisterDTO appUser)
        {
            if (ModelState.IsValid)
            {
                var result = _appUserService.Register(appUser).Result;
                if (result.IsSuccess)
                {
                    return Ok(result.Data);
                }

                return BadRequest(result.Message);
            }
            // DM Kien rua
            return BadRequest("Invalid Register");

        }

        [HttpPost]
        public IActionResult Login(AppUserLoginDTO appUser)
        {
            if (ModelState.IsValid)
            {
                var result = _appUserService.Login(appUser).Result;
                if (result.IsSuccess)
                {
                    var token = GenerateToken(result.Data);
                    return Ok(token);
                }

                return BadRequest(result.Message);
            }

            return BadRequest("Invalid Login");
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Private()
        {
            return Ok("Done");
        }

        private string GenerateToken(AppUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var secretKey = Encoding.ASCII.GetBytes(_optionsMonitor.Secret);
            var claims = GetAllValidClaims(user).Result;
            var DescriptionToken = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha256)

            };

            var newToken = jwtTokenHandler.CreateToken(DescriptionToken);
            var writeToken = jwtTokenHandler.WriteToken(newToken);
            return writeToken;
            //return new AuthResult
            //{
            //    Token = writeToken,
            //    RefreshToken = refreshToken.Token,
            //    Success = true
            //};
        }

        private async Task<List<Claim>> GetAllValidClaims(AppUser user)
        {
            var option = new IdentityOptions();
            var claims = new List<Claim>()
            {
                new Claim("id", user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                    //unique indified
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var userClaims = await _userManager.GetClaimsAsync(user);
            claims.AddRange(userClaims);

            // get user role and add it to the claim
            var userRoles = await _userManager.GetRolesAsync(user);

            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));

                var role = await _roleManager.FindByNameAsync(userRole);
                if (role != null)
                {
                    var roleClaims = await _roleManager.GetClaimsAsync(role);
                    foreach (var roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }
            return claims;
        }
    }
}
