﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using ProjectScheduleManagement.APIs.Mapper;
using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.Infrastructures;
using ProjectScheduleManagement.DTO.AppUsers;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.APIs.Services.AppUserServices 
{ 
    public class AppUserService : IAppUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ILogger<AppUserService> _logger;
        public AppUserService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<AppUserService> logger, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<Response<AppUser>> Login(AppUserLoginDTO appUser)
        {
            var response = new Response<AppUser>();
            var result = await _signInManager.PasswordSignInAsync(appUser.UserName, appUser.Password, appUser.RememberMe, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in.");
                var user = await _userManager.FindByNameAsync(appUser.UserName);
                if(user == null)
                {
                    response.IsSuccess = false;
                    response.Message = "Tai khoan hoac mat khau khong chinh xac";
                }
                else
                {
                    response.IsSuccess = true;
                    response.Message = "Dang nhap thanh cong";
                    response.Data = user;
                }
            }
            return response;
        }

        public async Task<Response<AppUserRegisterDTO>> Register(AppUserRegisterDTO appUser)
        {
            var response = new Response<AppUserRegisterDTO>();
            var user = MappingProfileGeneral<AppUserRegisterDTO, AppUser>.Map(appUser);
            //var user = new AppUser { UserName = appUser.UserName, Email = appUser.Email };
            var result = await _userManager.CreateAsync(user, appUser.Password);
            if (result.Succeeded)
            {
                response.IsSuccess = true;
                response.StatusCode = HttpStatusCode.Created;
                response.Message = "Dang ky thanh cong!";
                response.Data = appUser;
            }

            else
            {
                response.IsSuccess = false;
                response.Message = "Co loi xay ra trong qua trinh dang ky";
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            return response; ;
        }

        
    }
}
