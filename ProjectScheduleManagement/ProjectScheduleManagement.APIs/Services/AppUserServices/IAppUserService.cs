﻿using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.AppUsers;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.APIs.Services.AppUserServices
{
    public interface IAppUserService
    {
        Task<Response<AppUserRegisterDTO>> Register(AppUserRegisterDTO appUser);

        Task<Response<AppUser>> Login(AppUserLoginDTO appUser);
    }
}