﻿using AutoMapper;
using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.Infrastructures;
using ProjectScheduleManagement.DTO.Projects;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;

namespace ProjectScheduleManagement.APIs.Services.ProjectServices
{
    public class ProjectService : IProjectService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Response<ProjectInsertDTO> Add(ProjectInsertDTO entity)
        {
            try
            {
                _unitOfWork.ProjectRepository.Create(_mapper.Map<Project>(entity));
                _unitOfWork.SaveChanges();
                return new Response<ProjectInsertDTO>(true, HttpStatusCode.Created, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<ProjectInsertDTO>(false, HttpStatusCode.BadRequest, ex.Message, entity);
            }
        }

        public Response<bool> Delete(Guid key)
        {
            try
            {
                _unitOfWork.ProjectRepository.Delete(_unitOfWork.ProjectRepository.FindById(key));
                _unitOfWork.SaveChanges();
                return new Response<bool>(true, HttpStatusCode.OK, "success", true);
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, HttpStatusCode.BadRequest, ex.Message, true);
            }
        }

        public Response<List<ProjectDTO>> GetAll()
        {
            try
            {
                List<ProjectDTO> proDto = new List<ProjectDTO>();
                var pro = _unitOfWork.ProjectRepository.GetAll();
                foreach (var item in pro)
                {
                    proDto.Add(_mapper.Map<ProjectDTO>(item));
                }
                return new Response<List<ProjectDTO>>(true, HttpStatusCode.OK, "success", proDto);
            }
            catch (Exception ex)
            {
                return new Response<List<ProjectDTO>>(true, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }

        public Response<IEnumerable<ProjectDTO>> GetByFilterAndProps(Expression<Func<Project, bool>> filter = null, bool includeProperties = false )
        {
            try
            {
                var projects = _unitOfWork.ProjectRepository.FindAllByCondition(filter, includeProperties);
                IList<ProjectDTO> projectDtos = new List<ProjectDTO>();
                foreach (var order in projects)
                {
                    projectDtos.Add(_mapper.Map<ProjectDTO>(order));
                }
                return new Response<IEnumerable<ProjectDTO>>(true, HttpStatusCode.OK, "success", projectDtos);
            }
            catch (Exception ex)
            {
                return new Response<IEnumerable<ProjectDTO>>(false, HttpStatusCode.InternalServerError, ex.Message, null);
            }
        }
        public Response<ProjectDTO> GetById(Guid key)
        {
            try
            {
                var projects = _mapper.Map<ProjectDTO>(_unitOfWork.ProjectRepository.FindById(key));
                return new Response<ProjectDTO>(true, HttpStatusCode.OK, "sucess", projects);
            }
            catch (Exception ex)
            {
                return new Response<ProjectDTO>(false, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }

        public Response<ProjectUpdateDTO> Update(ProjectUpdateDTO entity)
        {
            try
            {
                _unitOfWork.ProjectRepository.Update(_mapper.Map<Project>(entity));
                _unitOfWork.SaveChanges();
                return new Response<ProjectUpdateDTO>(true, HttpStatusCode.Accepted, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<ProjectUpdateDTO>(false,HttpStatusCode.BadRequest,ex.Message, entity);
            }
        }
    }
}
