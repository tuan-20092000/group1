﻿using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Projects;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProjectScheduleManagement.APIs.Services.ProjectServices
{
    public interface IProjectService
    {
        Response<ProjectInsertDTO> Add(ProjectInsertDTO entity);

        Response<ProjectUpdateDTO> Update(ProjectUpdateDTO entity);

        Response<bool> Delete(Guid key);

        Response<List<ProjectDTO>> GetAll();

        Response<IEnumerable<ProjectDTO>> GetByFilterAndProps(Expression<Func<Project, bool>> filter = null, bool includeProperties = false);

        Response<ProjectDTO> GetById(Guid key);
    }
}
