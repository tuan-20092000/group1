﻿using AutoMapper;
using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.Infrastructures;
using ProjectScheduleManagement.DTO.Tasks;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;

namespace ProjectScheduleManagement.APIs.Services.TaskServices
{
    public class TaskService : ITaskService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public Response<List<TaskDisplayDTO>> GetAll()
        {
            throw new NotImplementedException();
        }
        public Response<List<TaskDisplayDTO>> GetAll(string projectId)
        {
            try
            {
                List<TaskDisplayDTO> taskDisplayDtos = new List<TaskDisplayDTO>();
                var tasks = _unitOfWork.TaskRepository.GetAll(projectId);
                foreach (var t in tasks)
                {
                    taskDisplayDtos.Add(_mapper.Map<TaskDisplayDTO>(t));
                }
                return new Response<List<TaskDisplayDTO>>(true, HttpStatusCode.OK, "success", taskDisplayDtos);
            }
            catch (Exception ex)
            {
                return new Response<List<TaskDisplayDTO>>(true, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }
        public Response<IEnumerable<TaskDisplayDTO>> GetByFilterAndProps(Expression<Func<Task, bool>> filter = null, bool includeProperties = false)
        {
            try
            {
                var tasks = _unitOfWork.TaskRepository.FindAllByCondition(filter, includeProperties);
                IList<TaskDisplayDTO> taskDisplayDTOs = new List<TaskDisplayDTO>();
                foreach (var t in tasks)
                {
                    taskDisplayDTOs.Add(_mapper.Map<TaskDisplayDTO>(t));
                }
                return new Response<IEnumerable<TaskDisplayDTO>>(true, HttpStatusCode.OK, "success", taskDisplayDTOs);
            }
            catch (Exception ex)
            {
                return new Response<IEnumerable<TaskDisplayDTO>>(false, HttpStatusCode.InternalServerError, ex.Message, null);
            }
        }
        public Response<TaskDisplayDTO> GetById(Guid key)
        {
            try
            {
                var tasks = _mapper.Map<TaskDisplayDTO>(_unitOfWork.TaskRepository.FindById(key));
                return new Response<TaskDisplayDTO>(true, HttpStatusCode.OK, "sucess", tasks);
            }
            catch (Exception ex)
            {
                return new Response<TaskDisplayDTO>(false, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }
        public Response<TaskCreateDTO> Add(TaskCreateDTO entity)
        {
            try
            {
                var task = _mapper.Map<Task>(entity);
                task.Id = Guid.NewGuid();
                task.Status = Common.StatusTasks.StatusTask.ToDo;
                _unitOfWork.TaskRepository.Create(task);
                _unitOfWork.SaveChanges();
                return new Response<TaskCreateDTO>(true, HttpStatusCode.Created, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<TaskCreateDTO>(false, HttpStatusCode.BadRequest, ex.Message, entity);
            }
        }
        public Response<TaskUpdateDTO> Update(TaskUpdateDTO entity)
        {
            try
            {
                _unitOfWork.TaskRepository.Update(_mapper.Map<Task>(entity));
                _unitOfWork.SaveChanges();
                return new Response<TaskUpdateDTO>(true, HttpStatusCode.Accepted, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<TaskUpdateDTO>(false, HttpStatusCode.BadRequest, ex.Message, entity);
            }
        }
        public Response<bool> Delete(Guid key)
        {
            try
            {
                _unitOfWork.TaskRepository.Delete(_unitOfWork.TaskRepository.FindById(key));
                _unitOfWork.SaveChanges();
                return new Response<bool>(true, HttpStatusCode.OK, "success", true);
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, HttpStatusCode.NotFound, ex.Message, true);
            }
        }

    }
}
