﻿using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Tasks;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProjectScheduleManagement.APIs.Services.TaskServices
{
    public interface ITaskService
    {
        Response<List<TaskDisplayDTO>> GetAll();
        Response<List<TaskDisplayDTO>> GetAll(string projectId);
        Response<IEnumerable<TaskDisplayDTO>> GetByFilterAndProps(Expression<Func<Task, bool>> filter = null, bool includeProperties = false);
        Response<TaskDisplayDTO> GetById(Guid key);
        Response<TaskCreateDTO> Add(TaskCreateDTO entity);
        Response<TaskUpdateDTO> Update(TaskUpdateDTO entity);
        Response<bool> Delete(Guid key);
    }
}
