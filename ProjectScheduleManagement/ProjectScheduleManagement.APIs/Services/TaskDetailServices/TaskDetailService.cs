﻿using AutoMapper;
using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.Infrastructures;
using ProjectScheduleManagement.DTO.TaskDetails;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;

namespace ProjectScheduleManagement.APIs.Services.TaskDetailServices
{
    public class TaskDetailService : ITaskDetailService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected IMapper _mapper;

        public TaskDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public Response<List<TaskDetailDisplayDTO>> GetAll()
        {
            throw new NotImplementedException();
        }
        public Response<List<TaskDetailDisplayDTO>> GetAll(string taskId)
        {
            try
            {
                List<TaskDetailDisplayDTO> taskDetailDisplayDtos = new List<TaskDetailDisplayDTO>();
                var taskDetails = _unitOfWork.TaskDetailRepository.GetAll(taskId);
                foreach (var td in taskDetails)
                {
                    taskDetailDisplayDtos.Add(_mapper.Map<TaskDetailDisplayDTO>(td));
                }
                return new Response<List<TaskDetailDisplayDTO>>(true, HttpStatusCode.OK, "success", taskDetailDisplayDtos);
            }
            catch (Exception ex)
            {
                return new Response<List<TaskDetailDisplayDTO>>(true, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }
        public Response<TaskDetailDisplayDTO> GetById(Guid id)
        {
            try
            {
                var taskDetails = _mapper.Map<TaskDetailDisplayDTO>(_unitOfWork.TaskDetailRepository.FindById(id));
                return new Response<TaskDetailDisplayDTO>(true, HttpStatusCode.OK, "sucess", taskDetails);
            }
            catch (Exception ex)
            {
                return new Response<TaskDetailDisplayDTO>(false, HttpStatusCode.BadRequest, ex.Message, null);
            }
        }
        public Response<IEnumerable<TaskDetailDisplayDTO>> GetByFilterAndProps(Expression<Func<TaskDetail, bool>> filter = null, bool includeProperties = false)
        {
            try
            {
                var taskDetails = _unitOfWork.TaskDetailRepository.FindAllByCondition(filter, includeProperties);
                IList<TaskDetailDisplayDTO> taskDetailDisplayDTOs = new List<TaskDetailDisplayDTO>();
                foreach (var t in taskDetails)
                {
                    taskDetailDisplayDTOs.Add(_mapper.Map<TaskDetailDisplayDTO>(t));
                }
                return new Response<IEnumerable<TaskDetailDisplayDTO>>(true, HttpStatusCode.OK, "success", taskDetailDisplayDTOs);
            }
            catch (Exception ex)
            {
                return new Response<IEnumerable<TaskDetailDisplayDTO>>(false, HttpStatusCode.InternalServerError, ex.Message, null);
            }
        }
        public Response<TaskDetailCreateDTO> Add(TaskDetailCreateDTO entity)
        {
            try
            {
                var taskDetail = _mapper.Map<TaskDetail>(entity);
                taskDetail.Id = Guid.NewGuid();
                _unitOfWork.TaskDetailRepository.Create(taskDetail);
                _unitOfWork.SaveChanges();
                return new Response<TaskDetailCreateDTO>(true, HttpStatusCode.Created, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<TaskDetailCreateDTO>(false, HttpStatusCode.BadRequest, ex.Message, entity);
            }
        }
        public Response<TaskDetailUpdateDTO> Update(TaskDetailUpdateDTO entity)
        {
            try
            {
                _unitOfWork.TaskDetailRepository.Update(_mapper.Map<TaskDetail>(entity));
                _unitOfWork.SaveChanges();
                return new Response<TaskDetailUpdateDTO>(true, HttpStatusCode.Accepted, "success", entity);
            }
            catch (Exception ex)
            {
                return new Response<TaskDetailUpdateDTO>(false, HttpStatusCode.BadRequest, ex.Message, entity);
            }
        }
        public Response<bool> Delete(Guid id)
        {
            try
            {
                _unitOfWork.TaskDetailRepository.Delete(_unitOfWork.TaskDetailRepository.FindById(id));
                _unitOfWork.SaveChanges();
                return new Response<bool>(true, HttpStatusCode.OK, "success", true);
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, HttpStatusCode.NotFound, ex.Message, true);
            }
        }

    }
}
