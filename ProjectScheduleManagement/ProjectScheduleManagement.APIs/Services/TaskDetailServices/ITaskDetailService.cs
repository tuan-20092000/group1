﻿using ProjectScheduleManagement.Common.Responses;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.TaskDetails;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProjectScheduleManagement.APIs.Services.TaskDetailServices
{
    public interface ITaskDetailService
    {
        Response<List<TaskDetailDisplayDTO>> GetAll();
        Response<List<TaskDetailDisplayDTO>> GetAll(string taskId);
        Response<IEnumerable<TaskDetailDisplayDTO>> GetByFilterAndProps(Expression<Func<TaskDetail, bool>> filter = null, bool includeProperties = false);
        Response<TaskDetailDisplayDTO> GetById(Guid key);
        Response<TaskDetailCreateDTO> Add(TaskDetailCreateDTO entity);
        Response<TaskDetailUpdateDTO> Update(TaskDetailUpdateDTO entity);
        Response<bool> Delete(Guid key);
    }
}
