﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectScheduleManagement.APIs.Services.AppUserServices;
using ProjectScheduleManagement.APIs.Services.ProjectServices;
using ProjectScheduleManagement.APIs.Services.TaskDetailServices;
using ProjectScheduleManagement.APIs.Services.TaskServices;
using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.DataAccessLayer.Infrastructures;

namespace ProjectScheduleManagement.APIs.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCORS(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                });
            });
        }
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer
            (
                configuration.GetConnectionString("sqlConnection"),
                b => b.MigrationsAssembly("ProjectScheduleManagement.APIs")
            ));
        }
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IAppUserService, AppUserService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITaskDetailService, TaskDetailService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
