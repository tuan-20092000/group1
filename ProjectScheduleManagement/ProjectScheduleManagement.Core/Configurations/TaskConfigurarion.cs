﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectScheduleManagement.Core.Domain;

namespace ProjectScheduleManagement.Core.Configurations
{
    public class TaskConfigurarion : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(255).IsRequired();
            builder.HasMany(t => t.TaskDetails).WithOne(p => p.Task).HasForeignKey(p => p.TaskId);
        }
    }
}
