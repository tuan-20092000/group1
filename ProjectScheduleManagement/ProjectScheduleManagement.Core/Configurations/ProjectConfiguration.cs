﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectScheduleManagement.Core.Domain;
using System;

namespace ProjectScheduleManagement.Core.Configurations
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.Property(p => p.Name)
                .HasMaxLength(255).IsRequired();
            builder.Property(p => p.Description).HasMaxLength(1255);
            builder.Property(p => p.StartDate).HasDefaultValue(DateTime.Now);
            builder.Property(p => p.EndDate).IsRequired();
            builder.HasMany(p => p.UserProjects)
                .WithOne(u => u.Project)
                .HasForeignKey(p => p.ProjectId);
            builder.HasMany(p => p.Tasks)
                .WithOne(u => u.Project)
                .HasForeignKey(p => p.ProjectId);
        }
    }
}
