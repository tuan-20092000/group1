﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectScheduleManagement.Core.Domain;

namespace ProjectScheduleManagement.Core.Configurations
{
    public class TaskDetailConfiguration : IEntityTypeConfiguration<TaskDetail>
    {
        public void Configure(EntityTypeBuilder<TaskDetail> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(255).IsRequired();
            builder.Property(t => t.Description).HasMaxLength(1255).IsRequired();
            builder.HasOne(t => t.AppUser).WithMany(u => u.TaskDetails).HasForeignKey(t => t.UserId);
        }
    }
}
