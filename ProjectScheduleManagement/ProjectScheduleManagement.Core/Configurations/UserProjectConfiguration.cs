﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectScheduleManagement.Core.Domain;

namespace ProjectScheduleManagement.Core.Configurations
{
    public class UserProjectConfiguration : IEntityTypeConfiguration<UserProject>
    {
        public void Configure(EntityTypeBuilder<UserProject> builder)
        {
            builder.HasKey(u => new { u.ProjectId, u.UserId });
            builder.HasOne(u => u.AppUser).WithMany(us => us.UserProjects).HasForeignKey(u => u.UserId);
        }
    }
}
