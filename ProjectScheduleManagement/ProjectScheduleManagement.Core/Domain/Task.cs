﻿using ProjectScheduleManagement.Common.StatusTasks;
using System;
using System.Collections.Generic;

namespace ProjectScheduleManagement.Core.Domain
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ProjectId { get; set; }
        public ICollection<TaskDetail> TaskDetails { get; set; }
        public Project Project { get; set; }
        public StatusTask Status { get; set; }
    }
}
