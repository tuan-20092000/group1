﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.Core.Domain
{
    public class AppUser : IdentityUser<Guid>
    {
        public Guid UserProjectId { get; set; }
        public Guid TaskDetailId { get; set; }
        public ICollection<UserProject> UserProjects { get; set; }
        public ICollection<TaskDetail> TaskDetails { get; set; }

        public byte[] Image { get; set; }
        public string Extend { get; set; }
    }
}
