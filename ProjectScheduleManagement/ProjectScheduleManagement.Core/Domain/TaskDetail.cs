﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.Core.Domain
{
    public class TaskDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? UserId { get; set; }
        public Guid TaskId { get; set; }
        public Task Task { get; set; }
        public AppUser AppUser { get; set; }
    }
}
