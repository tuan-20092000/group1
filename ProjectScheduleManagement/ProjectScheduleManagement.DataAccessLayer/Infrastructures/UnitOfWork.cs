﻿using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;
using ProjectScheduleManagement.DataAccessLayer.Repositories;

namespace ProjectScheduleManagement.DataAccessLayer.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        private IAppUserRepository _appUserRepository;
        private IProjectRepository _projectRepository;
        private IUserProjectRepository _userProjectRepository;
        private ITaskRepository _taskRepository;
        private ITaskDetailRepository _taskDetailRepository;
        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
        }
        public IAppUserRepository AppUserRepository => _appUserRepository ?? new AppUserRepository(_context);
        public IProjectRepository ProjectRepository => _projectRepository ?? new ProjectRepository(_context);
        public IUserProjectRepository UserProjectRepository => _userProjectRepository ?? new UserProjectRepository(_context);
        public ITaskRepository TaskRepository => _taskRepository ?? new TaskRepository(_context);
        public ITaskDetailRepository TaskDetailRepository => _taskDetailRepository ?? new TaskDetailReposioty(_context);
        public void Dispose()
        {
            _context.Dispose();
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
