﻿using ProjectScheduleManagement.DataAccessLayer.IRepositories;
using System;

namespace ProjectScheduleManagement.DataAccessLayer.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        IAppUserRepository AppUserRepository { get; }
        IProjectRepository ProjectRepository { get; }
        IUserProjectRepository UserProjectRepository { get; }
        ITaskRepository TaskRepository { get; }
        ITaskDetailRepository TaskDetailRepository { get; }
        void SaveChanges();
    }
}
