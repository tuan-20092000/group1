﻿using ProjectScheduleManagement.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface IUserProjectRepository : IBaseRepository<UserProject>
    {
    }
}
