﻿using ProjectScheduleManagement.Core.Domain;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface IProjectRepository : IBaseRepository<Project>
    {
    }
}
