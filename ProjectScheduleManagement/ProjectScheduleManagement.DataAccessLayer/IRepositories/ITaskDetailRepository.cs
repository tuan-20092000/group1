﻿using ProjectScheduleManagement.Core.Domain;
using System.Collections.Generic;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface ITaskDetailRepository : IBaseRepository<TaskDetail>
    {
        IEnumerable<TaskDetail> GetAll(string taskId);
    }
}
