﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface IBaseRepository<T>
    {
        IEnumerable<T> GetAll();
        IQueryable<T> FindAll(bool trackChanges);
        IQueryable<T> FindAllByCondition(Expression<Func<T, bool>> expression, bool trackChanges);
        T FindById(Guid Id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
