﻿using ProjectScheduleManagement.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface ITaskRepository : IBaseRepository<Task>
    {
        IEnumerable<Task> GetAll(string projectId);
    }
}
