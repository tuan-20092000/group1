﻿using ProjectScheduleManagement.Core.Domain;

namespace ProjectScheduleManagement.DataAccessLayer.IRepositories
{
    public interface IAppUserRepository : IBaseRepository<AppUser>
    {
    }
}
