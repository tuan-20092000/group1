﻿using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class TaskDetailReposioty : BaseRepository<TaskDetail>, ITaskDetailRepository
    {
        public TaskDetailReposioty(ApplicationContext context) : base(context)
        {
        }

        public IEnumerable<TaskDetail> GetAll(string taskId)
        {
            return _dbSet.Where(t => t.TaskId== Guid.Parse(taskId));
        }
    }
}
