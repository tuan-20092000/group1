﻿using Microsoft.EntityFrameworkCore;
using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        public TaskRepository(ApplicationContext context) : base(context)
        {
        }

        public IEnumerable<Task> GetAll(string projectId)
        {
            var tasks = _dbSet.Where(t => t.ProjectId == Guid.Parse(projectId));
            return tasks;
        }
    }
}
