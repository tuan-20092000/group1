﻿using Microsoft.EntityFrameworkCore;
using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected ApplicationContext _context;
        protected DbSet<T> _dbSet;
        public BaseRepository(ApplicationContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }
        public virtual IQueryable<T> FindAll(bool trackChanges) => !trackChanges ? _dbSet.AsNoTracking() : _dbSet;
        public virtual IQueryable<T> FindAllByCondition(Expression<Func<T, bool>> expression, bool trackChanges)
        {
            var entities = _dbSet.Where(expression);
            return !trackChanges ? entities.AsNoTracking() : entities;
        }
        //public T FindByCondition(Expression<Func<T, bool>> expression)
        //{
        //    return _dbSet.SingleOrDefault(expression);
        //}
        public virtual void Create(T entity) => _dbSet.Add(entity);
        public virtual void Update(T entity) => _dbSet.Update(entity);
        public virtual void Delete(T entity) => _dbSet.Remove(entity);
        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T FindById(Guid Id)
        {
            return _dbSet.Find(Id);
        }
    }
}
