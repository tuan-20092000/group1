﻿using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class AppUserRepository : BaseRepository<AppUser>, IAppUserRepository
    {
        public AppUserRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
