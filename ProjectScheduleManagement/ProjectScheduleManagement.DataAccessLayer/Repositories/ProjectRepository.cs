﻿using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
