﻿using ProjectScheduleManagement.Core.Context;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DataAccessLayer.IRepositories;

namespace ProjectScheduleManagement.DataAccessLayer.Repositories
{
    public class UserProjectRepository : BaseRepository<UserProject>, IUserProjectRepository
    {
        public UserProjectRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
