﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Projects;
using ProjectScheduleManagement.UI.Models;
using ProjectScheduleManagement.UI.Services.ProjectServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProjectService _projectService;

        public HomeController(ILogger<HomeController> logger, IProjectService projectService)
        {
            _logger = logger;
            _projectService = projectService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _projectService.GetAllAsync());
        }
        public IActionResult Project()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateAsync(ProjectInsertDTO project,string name)
        {
            if (ModelState.IsValid)
            {
                await _projectService.CreateAsync(project);
            }
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string Id)
        {
            if (ModelState.IsValid)
            {
                await _projectService.DeleteAsync(Id);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> Update(string Id)
        {
            if (ModelState.IsValid)
            {
                await _projectService.UpdateAsync(Id);
            }
            return RedirectToAction("Index");
        }
    }
}
