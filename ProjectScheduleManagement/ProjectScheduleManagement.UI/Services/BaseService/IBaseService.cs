﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.UI.Services.BaseService
{
    public interface IBaseService<T> where T : class
    {
        Task<T> GetAsync(string url, Guid id, string token);

        Task<IList<T>> GetAllAsync(string url, string token);

        Task<bool> CreateAsync(string url, T entity, string token);

        Task<bool> UpdateAsync(string url, T entity, string token);

        Task<bool> DeleteAsync(string url, Guid id, string token);
    }
}
