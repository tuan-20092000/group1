﻿using Newtonsoft.Json;
using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Projects;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.UI.Services.ProjectServices
{
    public class ProjectService : IProjectService
    {
        public async Task<bool> CreateAsync(ProjectInsertDTO project)
        {
            using (var client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(project),Encoding.UTF8,"application/json");
                using (var response = await client.PostAsync("https://localhost:44347/api/Project/Create",content))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        await response.Content.ReadAsStringAsync();
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> DeleteAsync(string id)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync($"https://localhost:44347/api/Project/Delete/{id}"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        await response.Content.ReadAsStringAsync();
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<IList<ProjectDTO>> GetAllAsync()
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync("https://localhost:44347/api/Project/GetAll"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var jsonStr = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<IList<ProjectDTO>>(jsonStr);
                    }
                }
            }
            return null;
        }

        public async Task<bool> UpdateAsync(string id)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync($"https://localhost:44347/api/Project/Update/{id}"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var jsonStr = await response.Content.ReadAsStringAsync();
                        JsonConvert.DeserializeObject<ProjectUpdateDTO>(jsonStr);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
