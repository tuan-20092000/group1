﻿using ProjectScheduleManagement.Core.Domain;
using ProjectScheduleManagement.DTO.Projects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.UI.Services.ProjectServices
{
    public interface IProjectService
    {
        Task<IList<ProjectDTO>> GetAllAsync();
        Task<bool> CreateAsync(ProjectInsertDTO project);

        Task<bool> UpdateAsync(string id);

        Task<bool> DeleteAsync(string id);
    }
}
