﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.DTO.Tasks
{
    public class TaskCreateDTO
    {
        public string Name { get; set; }
        public Guid ProjectId { get; set; }
    }
}
