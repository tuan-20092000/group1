﻿using ProjectScheduleManagement.Common.StatusTasks;
using System;

namespace ProjectScheduleManagement.DTO.Tasks
{
    public class TaskDisplayDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public StatusTask Status { get; set; }
    }
}
