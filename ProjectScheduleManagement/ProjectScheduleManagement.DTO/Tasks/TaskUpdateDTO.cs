﻿using ProjectScheduleManagement.Common.StatusTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectScheduleManagement.DTO.Tasks
{
    public class TaskUpdateDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ProjectId { get; set; }
        public StatusTask Status { get; set; }
    }
}
