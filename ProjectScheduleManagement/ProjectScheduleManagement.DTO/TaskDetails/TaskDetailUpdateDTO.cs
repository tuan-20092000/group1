﻿using System;

namespace ProjectScheduleManagement.DTO.TaskDetails
{
    public class TaskDetailUpdateDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? UserId { get; set; }
        public Guid TaskId { get; set; }
    }
}
